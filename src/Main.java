import java.io.*;
import java.io.PrintStream;
import java.util.*;

public class Main
{
    public static void main(final String[] args)
    {
        BufferedReader reader = null;
        SortedMap<String,Account> sortMap = new TreeMap<>();

        try {
            File file = new File("rawData.csv");
            reader = new BufferedReader(new FileReader(file));

            String line;
            while ((line = reader.readLine()) != null) {
                String str = line;
                String [] arrOfStr = str.split(",", 5);

                //sortMap.get(arrOfStr[1]);
                if (!arrOfStr[0].equals("accountNumber")) {
                    if (!sortMap.containsKey(arrOfStr[0])) {

                        //System.out.println(arrOfStr[0]);
                        //System.out.println(arrOfStr[1]);
                        //System.out.println(arrOfStr[2]);
                        //System.out.println(arrOfStr[3]);
                        Account acc = new Account(arrOfStr[0], arrOfStr[1], arrOfStr[2], arrOfStr[3]);
                        sortMap.put(arrOfStr[0], acc);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for (Map.Entry<String, Account> entry : sortMap.entrySet()) {
            System.out.println("Key: " + entry.getKey() + " Value: " + entry.getValue().toString());
        }
    }
}
