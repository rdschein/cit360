public class Account {
    public String accountNumber;
    public String accountType;
    public String givenName;
    public String familyName;

    //Constructors
    public Account() {
        setAccountNumber("null");
        setAccountType("null");
        setGivenName("null");
        setFamilyName("null");
    }
    public Account(String accNum, String accType, String gName, String fName) {
        setAccountNumber(accNum);
        setAccountType(accType);
        setGivenName(gName);
        setFamilyName(fName);
    }

    //Getters
    public String getAccountNumber() {
        return accountNumber;
    }
    public String getAccountType() {
        return accountType;
    }
    public String getGivenName() {
        return givenName;
    }
    public String getFamilyName() {
        return familyName;
    }
    //Setters
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }
    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }
    public String toString() {
        String str = getAccountType() + " " + getGivenName() + " " + getFamilyName();
        return str;
    }
}
