public class DogBean extends MammalBean {

    private String breed;
    private String name;

    DogBean(){
    }


    DogBean(String breed, String name, int legCount, String color, double height) {
        super.setLegCount(legCount);
        super.setColor(color);
        super.setHeight(height);
        this.setBreed(breed);
        this.setName(name);
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "Dog Properties: " + "Name: " + name + " Breed: " + breed + " LegCount: " + getLegCount() + " Color: " + getColor() + " Height: " + getHeight();
    }
}
