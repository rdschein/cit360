public class Kennel {

    public Kennel() {
    }

    public DogBean[] buildDogs() {

        DogBean dogOne = new DogBean("Black Lab", "Fred", 4, "Black", 5.5);
        DogBean dogTwo = new DogBean("Golden Retriever", "Jack", 4, "Gold", 3.1);
        DogBean dogThree = new DogBean("Husky", "Pal", 4, "Grey", 4);
        DogBean dogFour = new DogBean("Corgie", "Buddy", 4, "Yellow", 2.3);
        DogBean dogFive = new DogBean("Dalmation", "Spot", 4, "White and Black", 4.3);

        DogBean[] dogs = new DogBean[5];
        dogs[0] = dogOne;
        dogs[1] = dogTwo;
        dogs[2] = dogThree;
        dogs[3] = dogFour;
        dogs[4] = dogFive;
        return dogs;
    }

    public void displayDogs(DogBean[] dogs) {
        for (int i=0; i<dogs.length; i++) {
            dogs[i].toString();
            System.out.println(dogs[i].toString());
        }
    }

    public static void main(String[] args) {
        Kennel newKennel = new Kennel();
        DogBean[] allDogs = newKennel.buildDogs();
        newKennel.displayDogs(allDogs);
    }
}
