import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertEquals;

public class MammalTest {

    @Test
    public void testColor() {
        MammalBean m1 = new MammalBean(4, "Black", 5);
        assertEquals("Black", m1.getColor());
        MammalBean m2 = new MammalBean(3, "Red", 3);
        assertEquals("Red", m2.getColor());
        MammalBean m3 = new MammalBean(2, "Black", 4);
        assertEquals("Black", m3.getColor());
    }
    @Test
    public void testLegCount() {
        MammalBean m1 = new MammalBean(4, "Brown", 4.5);
        assertEquals(4, m1.getLegCount());
        MammalBean m2 = new MammalBean(6, "Green", 3.2);
        assertEquals(6, m2.getLegCount());
        MammalBean m3 = new MammalBean(2, "Tan", 6.4);
        assertEquals(2, m3.getLegCount());
    }
    @Test
    public void testHeight() {
        double delta = .000001;
        MammalBean m1 = new MammalBean(4, "Brown", 4.5);
        assertEquals(4.5, m1.getHeight(), delta);
        MammalBean m2 = new MammalBean(4, "Brown", 3);
        assertEquals(3, m2.getHeight(), delta);
        MammalBean m3 = new MammalBean(6, "Red", 5.4);
        assertEquals(5.4, m3.getHeight(), delta);
    }

    @Test
    public void testObjectType() {

        MammalBean m1 = new MammalBean(4, "Red", 4.3);
        MammalBean m2 = new MammalBean(5, "Black", 3.2);
        MammalBean m3 = new MammalBean(3, "Brown", 1.1);

        Set<MammalBean> mammalSet = new HashSet<>();
        mammalSet.add(m1);
        mammalSet.add(m2);
        mammalSet.add(m3);
        System.out.println("Set Size: " + mammalSet.size());
        //mammalSet.remove(m3);

        // for each
        for (MammalBean mammal : mammalSet) {
            assertTrue(mammalSet.contains(m1));
            assertTrue(mammalSet.contains(m2));
            assertTrue(mammalSet.contains(m3));
        }
    }
}
