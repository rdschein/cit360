import org.junit.Test;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import static org.hamcrest.core.IsAnything.anything;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertTrue;

public class DogTest {

    @Test
    public void testBreed() {
        DogBean d1 = new DogBean("Husky", "Jack", 4, "Grey", 3.2);
        assertEquals("Husky", d1.getBreed());
        DogBean d2 = new DogBean("Golden Retriever", "Bucky", 4, "Gold", 1.2);
        assertEquals("Golden Retriever", d2.getBreed());
        DogBean d3 = new DogBean("Great Dane", "Buddy", 4, "Gold", 1.2);
        assertEquals("Great Dane", d3.getBreed());
    }

    @Test
    public void testName() {
        DogBean d1 = new DogBean("Husky", "Jack", 4, "Grey", 3.2);
        assertEquals("Jack", d1.getName());
        DogBean d2 = new DogBean("Golden Retriever", "Bucky", 4, "Gold", 1.2);
        assertEquals("Bucky", d2.getName());
        DogBean d3 = new DogBean("Great Dane", "Buddy", 4, "Gold", 1.2);
        assertEquals("Buddy", d3.getName());
    }

    @Test
    public void testMap()

    {
        Map<String, DogBean> dogMap = new HashMap<>();

        DogBean d1 = new DogBean("Husky", "Jack", 4, "Grey", 3.2);
        DogBean d2 = new DogBean("Golden Retriever", "Bucky", 4, "Gold", 1.2);
        DogBean d3 = new DogBean("Great Dane", "Buddy", 4, "Gold", 1.2);

        dogMap.put(d1.getName(), d1);
        dogMap.put(d2.getName(), d2);
        dogMap.put(d3.getName(), d3);

        //dogMap.remove(d1.getName());
        assertTrue(dogMap.containsKey(d1.getName()));
        assertTrue(dogMap.containsKey(d2.getName()));
        assertTrue(dogMap.containsKey(d3.getName()));
    }
}

